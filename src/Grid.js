export default class Grid {
  constructor() {
    this.cells = [
      ['', '', ''],
      ['', '', ''],
      ['', '', ''],
    ];
    this.playerMark = 'X';
    this.computerMark = 'O';
  };

  copyCurrentGrid(grid) {
    let newGrid = new Grid();
    for (let row = 0; row < 3; row++) {
      for (let column = 0; column < 3; column++) {
        newGrid.cells[row][column] = grid.cells[row][column];
      }
    }
    return newGrid;
  };

  markMove(row, column, player) {
    this.cells[row][column] = player;
  };

  noPossibleMoves() {
    for (let row = 0; row < 3; row++) {
      for (let column = 0; column < 3; column++) {
        if (this.cells[row][column] === '') return false;
      }
    }
    return true;
  };

  getPossibleMoves() {
    let moves = [];
    for (let row = 0; row < 3; row++) {
      for (let column = 0; column < 3; column++) {
        if (this.cells[row][column] == '') moves.push({ row: row, column: column })
      }
    }
    return moves;
  };

  checkForWin(player) {
    for (let row = 0; row < 3; row++) {
      if (this.cells[row][0] == player && this.cells[row][1] == player && this.cells[row][2] == player) return true;
    }

    for (let column = 0; column < 3; column++) {
      if (this.cells[0][column] == player && this.cells[1][column] == player && this.cells[2][column] == player) return true;
    }

    if (this.cells[0][0] == player && this.cells[1][1] == player && this.cells[2][2] == player) return true;
    if (this.cells[0][2] == player && this.cells[1][1] == player && this.cells[2][0] == player) return true;

    return false;
  };

  isGameOver() {
    return this.noPossibleMoves() || this.checkForWin(this.playerMark) || this.checkForWin(this.computerMark);
  };

  score(depth = 1) {
    if (this.checkForWin(this.computerMark)) return 10 - depth;
    else if (this.checkForWin(this.playerMark)) return depth - 10;
    else return 0;
  };

  minimax(grid, activePlayer, depth = 1) {
    if (grid.isGameOver()) return { score: grid.score(depth), move: null };

    let possibleMoves = grid.getPossibleMoves();
    let bestScore = activePlayer === this.computerMark ? -100 : 100;
    let bestMoveChoice = null;

    possibleMoves.forEach((possibleMove) => {
      let newGrid = this.copyCurrentGrid(grid);
      newGrid.markMove(possibleMove.row, possibleMove.column, activePlayer);

      let newActivePlayer = activePlayer == this.computerMark ? this.playerMark : this.computerMark;
      let score = this.minimax(newGrid, newActivePlayer, depth + 1).score;

      if ((activePlayer === this.playerMark && score < bestScore) ||
        (activePlayer === this.computerMark && score > bestScore)) {
        bestScore = score;
        bestMoveChoice = possibleMove;
      }
    })

    return { score: bestScore, move: bestMoveChoice };
  }
}
