import Vue from 'vue'
import App from './App.vue'

import TicTacToe from './TicTacToe';
import Cell from './Cell';

Vue.component('TicTacToe', TicTacToe);
Vue.component('Cell', Cell);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
